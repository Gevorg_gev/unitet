﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Serialization
{
    public class Student
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; } 
    }
}
